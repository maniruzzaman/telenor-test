<?php

namespace App\Http\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class APIService
{
    public $expiry_ttl; 

    public function __construct($expiry_ttl=5) {
        date_default_timezone_set("Asia/Dhaka");
        $this->expiry_ttl = $expiry_ttl = 5; // 5 minutes as a given ttl 
        
        // Delete keys which expires TTL
        $this->delete();
    }

    /**
     * getTTLValue
     * 
     * @return int $expiry_ttl -> in minutes
     */
    public function getTTLValue()
    {
        return $this->expiry_ttl;
    }

    /**
     * getKeys
     * 
     * Get all the keys
     * 
     * @return array $keys
     */
    public function getKeys($updateTTL=true)
    {
        try {
            $keys = DB::table('keys')
                ->select('key', 'value', 'expiry_date_time')
                ->get();
            if($updateTTL){
                $this->updateTTL($keys);
            }
        } catch (\Exception $e) {
            $keys = [];
        }

        return $keys;
    }

    /**
     * updateTTL
     * 
     * @return 
     */
    public function updateTTL($keys)
    {
        $keysArray = [];
        $is_updated = false;

        try {
            foreach ($keys as $key) {
                array_push($keysArray, $key->key);
            }
    
            $is_updated = DB::table('keys')
                        ->whereIn('keys.key', $keysArray)
                        ->update([
                            'expiry_date_time' =>  Carbon::now()->addMinutes($this->expiry_ttl)
                    ]);
        } catch (\Exception $e) {
            $is_updated = false;
        }
        return $is_updated;
    }

    /**
     * printKeys
     * 
     * Print the key list as a string
     * 
     * @return string $keyList
     */
    public function printKeys($keysArray)
    {
        $keys = $keysArray;
        $keyList = "{";
        
        try {
            $i = 1;
            foreach ($keys as $key) {
                $keyList .=  $key->key . ': ' . $key->value;

                if($i != count($keys)){
                    $keyList .= ', ';
                }
                $i++;
            }
        } catch (\Exception $e) {
            // 
        }

        $keyList .= "}";
        return $keyList;
    }

    /**
     * getSingleKey
     * 
     * Get the single key by its key name
     * 
     * @return array $keySingleArray
     */
    public function getSingleKey($key)
    {
        $keySingleArray = [];
        try {
            $keySingleArray = DB::table('keys')
                ->where('key', $key)
                ->select('key', 'value')
                ->get();
            $this->updateTTL($keys);
        } catch (\Exception $e) {
            // 
        }
        return $keySingleArray;
    }

    /**
     * store
     * 
     * Create New Key value pairs
     * 
     * @return array $response
     */
    public function store($requests)
    {
        $inserted = false;
        
        try {
            foreach ($requests as $key =>  $value) {
                $keyObject = DB::table('keys')->where('key', $key)->first();
                if (is_null($keyObject)) {
                    DB::table('keys')->insert([
                        'key' => $key,
                        'value' => $value,
                        'created_at' => Carbon::now(),
                        'expiry_date_time' =>  Carbon::now()->addMinutes($this->expiry_ttl)
                    ]);
                }else{
                    // Update key value and ttl
                    $this->updateSingleKey($key, $value);
                }
            }
            $inserted = true;
        } catch (\Exception $e) {
            $inserted = false;
        }
        return $inserted;
    }

    /**
     * updateSingleKey
     * 
     * Update a key by its value and update ttl 5 minutes
     * 
     * @param string $key
     * @param string $value
     * 
     * @return bool $updated
     */
    public function updateSingleKey($key, $value)
    {
        $updated = false;
                
        try {
            $keyObject = DB::table('keys')->where('key', $key)->first();

            if (!is_null($keyObject)) {
                DB::table('keys')
                ->where('key', $key)
                ->update([
                    'value' => $value,
                    'updated_at' => Carbon::now(),
                    'expiry_date_time' =>  Carbon::now()->addMinutes($this->expiry_ttl)
                ]);
                $updated = true;
            }
        } catch (\Exception $e) {
            $updated = false;
        }
        return $updated;
    }

    
    /**
     * update
     * 
     * Update keys by its value and update ttl 5 minutes
     * 
     * @param array $requests
     * 
     * @return bool $updated
     */
    public function update($requests)
    {
        $updated = false;
        
        try {
            foreach ($requests as $key =>  $value) {
                $keyObject = DB::table('keys')->where('key', $key)->first();
                if (!is_null($keyObject)) {
                    $this->updateSingleKey($key, $value);
                }
            }
            $updated = true;
        } catch (\Exception $e) {
            $updated = false;
        }
        return $updated;
    }

    /**
     * delete
     * 
     * delete keys by its value and delete ttl 5 minutes
     * @return bool $deleted
     */
    public function delete()
    {
        $deleted = false;
        
        try {
            $keys = $this->getKeys(false);
            foreach ($keys as $keySingle) {
                if (Carbon::now() > $keySingle->expiry_date_time) {
                    $deleted = $this->deleteSingleKey($keySingle->key);
                }
            }
        } catch (\Exception $e) {
            $deleted = false;
        }
        return $deleted;
    }

    /**
     * deleteSingleKey
     * 
     * @param string $key 
     * @return bool $deleted Deleted or not
     */
    public function deleteSingleKey($key)
    {
        $deleted = false;
        
        try {
            $keySingle = DB::table('keys')->where('key', $key)->first();
            if(!is_null($keySingle)){
                DB::table('keys')->where('key', $key)->delete();
                $deleted = true;
            }
        } catch (\Exception $e) {
            $deleted = false;
        }
        return $deleted;
    }
}
