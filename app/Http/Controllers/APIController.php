<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\APIService;

use App\Jobs\KeyRefresherJob;

class APIController extends Controller
{

    public $apiService; 

    public function __construct(APIService $apiService) {
        $this->apiService = $apiService;
    }

    /**
     * index
     * 
     * Get all the values of the store
     * 
     * @return object json_encoded result
     */
    public function index(Request $request)
    {
        if (!isset($request->keys)) {
            $keyList = $this->apiService->printKeys($this->apiService->getKeys());
            return json_encode(['status' => true, 'message' => 'Keys List', 'keys' => $keyList]);
        }else{
            $keysRequested = $request->keys;
            return $this->show($keysRequested);
        }
    }

    /**
     * show
     * 
     * @return object json_encoded result
     */
    public function show($keysRequested)
    {
        $status = false;
        $message = "";
        $keys = [];

        $arrays = explode(',', $keysRequested);
        if(count($arrays) > 0){
            $newKeysArray = [];
            for ($i=0; $i < count($arrays); $i++) {
                $key = $arrays[$i];
                $singleItem = $this->apiService->getSingleKey($key);
                foreach ($singleItem as $item) {
                    array_push($newKeysArray, $item);
                }
            }
            $keys = $this->apiService->printKeys($newKeysArray);
            $status = true;
            $message = "Keys List";
        }else{
            $message = "Please specify minimum one key"; 
        }

        return json_encode(['status' => $status, 'message' => $message, 'keys' => $keys]);
    }

    /**
     * store
     * 
     * Create new key=>value pairs
     * 
     * @return array response
     */
    public function store(Request $request)
    {
        $keys = [];
        $status = false;
        $message = "";

        try {
            $isAdded = $this->apiService->store($request->all());
            if ($isAdded) {
                $status = true;
                $message = 'Keys have been created or updated !!';
                $keys = $this->apiService->printKeys($this->apiService->getKeys());
            }else{
                $message = 'Error Creating keys ! Fix the errors';
            }
        } catch (\Exception $e) {
            $status = false;
            $message = $e->getMessage();
        }
        return json_encode(['status' => $status, 'message' => $message, 'keys' => $keys]);
    }

    /**
     * update
     * 
     * Update new key=>value pairs
     * 
     * @return array response
     */
    public function update(Request $request)
    {
        $keys = [];
        $status = false;
        $message = "";

        try {
            $isUpdated = $this->apiService->update($request->all());
            
            if ($isUpdated) {
                $status = true;
                $message = 'Keys have been updated !!';
                $keys = $this->apiService->printKeys($this->apiService->getKeys());
            }else{
                $message = 'Error Updating keys ! Fix the errors';
            }
        } catch (\Exception $e) {
            $status = false;
            $message = $e->getMessage();
        }
        return json_encode(['status' => $status, 'message' => $message, 'keys' => $keys]);
    }

    public function delete()
    {
        $keys = [];
        $status = false;
        $message = "";

        try {
            $isDeleted = $this->apiService->delete();
            
            if ($isDeleted) {
                $status = true;
                $message = 'Keys (Over TTL) have been deleted !!';
                $keys = $this->apiService->printKeys($this->apiService->getKeys());
            }
        } catch (\Exception $e) {
            $status = false;
            $message = $e->getMessage();
        }
        return json_encode(['status' => $status, 'message' => $message, 'keys' => $keys]);
    }
}
