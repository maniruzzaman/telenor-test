<?php

namespace App\Jobs;


use Illuminate\Http\Request;
use App\Http\Services\APIService;
use Carbon\Carbon;

/**
 * KeyRefresherJob
 * 
 * Delete keys after its ttl time
 */

class KeyRefresherJob extends Job
{

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $apiService = new APIService();

        // Delete keys which expires TTL
        $apiService->delete();
    }
}
