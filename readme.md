# ReST Key-Value Store
---
Create an API with the following endpoints to create a key-value store. The purpose of the API is to store any arbitrary length value in a persistent store with respect to a key and later fetch these values by keys. These values will have a TTL (for example 5 minutes) and after the TTL is over, the values will be removed from the store.

## Built With
---
#### Lumen PHP Framework


[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

### Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## How to Start
---

#### Step 1

Clone the project - 
```bash
git clone https://maniruzzaman@bitbucket.org/maniruzzaman/telenor-test.git
```


#### Step 2

Connect with database in the `.env `- 
```php
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=telenor_api //DB name
DB_USERNAME=root // DB username
DB_PASSWORD= // DB password
```
set queue connection as database

```bash
QUEUE_CONNECTION=database
```

#### Step 3

Complete Migration - 
```php
php artisan migrate
```

#### Step 4

Run the Application at `8000` port & start the queue - 

```php
php -S localhost:8000 -t public
```

Run Queue Job - 
```php
php artisan queue:work --queue=high,default
```


## How to Use
---

> Every methods (`get`, `post`, `patch`)  will get/store/patch data if TTL does not expires in that time

#### Store Keys 

Save a value in the store.

URL `(post)` :
```php
http://localhost:8000/store
```

Input Data :
```json
{
    "key1": "8912u9182u981212bjh12bjh12j1243434341111111",
    "key2": "123343423232332323322434545454534353434"
}
```

Output Response :
```json
{
    "status": true,
    "message": "Keys have been created or updated !!",
    "keys": "{key1: 8912u9182u981212bjh12bjh12j1243434341111111, key2: 123343423232332323322434545454534353434}"
}
```



#### Get Keys - all

Get all the values of the store.

URL `(get)` :
```php
http://localhost:8000/values
```

Output Response :
```json
{
    "status": true,
    "message": "Keys List",
    "keys": "{key1: 8912u9182u981212bjh12bjh12j1243434341111111, key2: 123343423232332323322434545454534353434}"
}
```



#### Get Keys - search

Get one or more specific values from the store and also reset the TTL of those keys.

URL `(get)` :
```php
http://localhost:8000/values?keys=key1,key2
```

Output Response :
```json
{
    "status": true,
    "message": "Keys List",
    "keys": "{key1: 8912u9182u981212bjh12bjh12j1243434341111111, key2: 123343423232332323322434545454534353434}"
}
```



#### Update Keys -

Update a value in the store and also reset the TTL.


URL `(patch)` :
```php
http://localhost:8000/values
```

Input Data :
```json
{
    "key1": "8912u9182u981212bjh12bjh12j1243434340000000",
    "key2": "901290192012019280198298328930918098080001"
}
```

Output Response :
```json
{
    "status": true,
    "message": "Keys have been updated !!",
    "keys": "{key1: 8912u9182u981212bjh12bjh12j1243434340000000, key2: 901290192012019280198298328930918098080001}"
}
```

## Any issues
---
Add in issues or mail me at manirujjamanakash@gmail.com