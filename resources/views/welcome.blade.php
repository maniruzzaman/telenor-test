
<!DOCTYPE html>
  <html>
    <head>
        <title>ReST Key-Value Store - Maniruzzaman Akash</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body for="html-export" >
      <div class="mume markdown-preview  "  >
      <h1 class="mume-header" id="rest-key-value-store">ReST Key-Value Store</h1>

<hr>
<p>Create an API with the following endpoints to create a key-value store. The purpose of the API is to store any arbitrary length value in a persistent store with respect to a key and later fetch these values by keys. These values will have a TTL (for example 5 minutes) and after the TTL is over, the values will be removed from the store.</p>
<h2 class="mume-header" id="built-with">Built With</h2>

<hr>
<h4 class="mume-header" id="lumen-php-framework">Lumen PHP Framework</h4>

<p><a href="https://travis-ci.org/laravel/lumen-framework"><img src="https://travis-ci.org/laravel/lumen-framework.svg" alt="Build Status"></a><br>
<a href="https://packagist.org/packages/laravel/lumen-framework"><img src="https://poser.pugx.org/laravel/lumen-framework/d/total.svg" alt="Total Downloads"></a><br>
<a href="https://packagist.org/packages/laravel/lumen-framework"><img src="https://poser.pugx.org/laravel/lumen-framework/v/stable.svg" alt="Latest Stable Version"></a><br>
<a href="https://packagist.org/packages/laravel/lumen-framework"><img src="https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg" alt="Latest Unstable Version"></a><br>
<a href="https://packagist.org/packages/laravel/lumen-framework"><img src="https://poser.pugx.org/laravel/lumen-framework/license.svg" alt="License"></a></p>
<p>Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.</p>
<h3 class="mume-header" id="official-documentation">Official Documentation</h3>

<p>Documentation for the framework can be found on the <a href="https://lumen.laravel.com/docs">Lumen website</a>.</p>
<h2 class="mume-header" id="how-to-start">How to Start</h2>

<hr>
<h4 class="mume-header" id="step-1">Step 1</h4>

<p>Clone the project -</p>
<pre data-role="codeBlock" data-info="bash" class="language-bash"><span class="token function">git</span> clone https://maniruzzaman@bitbucket.org/maniruzzaman/telenor-test.git
</pre><h4 class="mume-header" id="step-2">Step 2</h4>

<p>Connect with database in the .env -</p>
<pre data-role="codeBlock" data-info="php" class="language-php"><span class="token constant">DB_CONNECTION</span><span class="token operator">=</span>mysql
<span class="token constant">DB_HOST</span><span class="token operator">=</span><span class="token number">127.0</span><span class="token number">.0</span><span class="token number">.1</span>
<span class="token constant">DB_PORT</span><span class="token operator">=</span><span class="token number">3306</span>
<span class="token constant">DB_DATABASE</span><span class="token operator">=</span>telenor_api <span class="token comment">//DB name</span>
<span class="token constant">DB_USERNAME</span><span class="token operator">=</span>root <span class="token comment">// DB username</span>
<span class="token constant">DB_PASSWORD</span><span class="token operator">=</span> <span class="token comment">// DB password</span>
</pre><h4 class="mume-header" id="step-3">Step 3</h4>

<p>Complete Migration -</p>
<pre data-role="codeBlock" data-info="php" class="language-php">php artisan migrate
</pre><h4 class="mume-header" id="step-4">Step 4</h4>

<p>Run the Application at <code>8000</code> port &amp; start the queue -</p>
<pre data-role="codeBlock" data-info="php" class="language-php">php <span class="token operator">-</span>S localhost<span class="token punctuation">:</span><span class="token number">8000</span> <span class="token operator">-</span>t <span class="token keyword">public</span>
</pre><p>Run Queue Job -</p>
<pre data-role="codeBlock" data-info="php" class="language-php">php artisan queue<span class="token punctuation">:</span>work <span class="token operator">--</span>queue<span class="token operator">=</span>high<span class="token punctuation">,</span><span class="token keyword">default</span>
</pre><h2 class="mume-header" id="how-to-use">How to Use</h2>

<hr>
<blockquote>
<p>Every methods (<code>get</code>, <code>post</code>, <code>patch</code>)  will get/store/patch data if TTL does not expires in that time</p>
</blockquote>
<h4 class="mume-header" id="store-keys">Store Keys</h4>

<p>Save a value in the store.</p>
<p>URL <code>(post)</code> :</p>
<pre data-role="codeBlock" data-info="php" class="language-php">http<span class="token punctuation">:</span><span class="token comment">//localhost:8000/store</span>
</pre><p>Input Data :</p>
<pre data-role="codeBlock" data-info="json" class="language-json"><span class="token punctuation">{</span>
    <span class="token property">&quot;key1&quot;</span><span class="token operator">:</span> <span class="token string">&quot;8912u9182u981212bjh12bjh12j1243434341111111&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;key2&quot;</span><span class="token operator">:</span> <span class="token string">&quot;123343423232332323322434545454534353434&quot;</span>
<span class="token punctuation">}</span>
</pre><p>Output Response :</p>
<pre data-role="codeBlock" data-info="json" class="language-json"><span class="token punctuation">{</span>
    <span class="token property">&quot;status&quot;</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">&quot;message&quot;</span><span class="token operator">:</span> <span class="token string">&quot;Keys have been created or updated !!&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;keys&quot;</span><span class="token operator">:</span> <span class="token string">&quot;{key1: 8912u9182u981212bjh12bjh12j1243434341111111, key2: 123343423232332323322434545454534353434}&quot;</span>
<span class="token punctuation">}</span>
</pre><h4 class="mume-header" id="get-keys-all">Get Keys - all</h4>

<p>Get all the values of the store.</p>
<p>URL <code>(get)</code> :</p>
<pre data-role="codeBlock" data-info="php" class="language-php">http<span class="token punctuation">:</span><span class="token comment">//localhost:8000/values</span>
</pre><p>Output Response :</p>
<pre data-role="codeBlock" data-info="json" class="language-json"><span class="token punctuation">{</span>
    <span class="token property">&quot;status&quot;</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">&quot;message&quot;</span><span class="token operator">:</span> <span class="token string">&quot;Keys List&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;keys&quot;</span><span class="token operator">:</span> <span class="token string">&quot;{key1: 8912u9182u981212bjh12bjh12j1243434341111111, key2: 123343423232332323322434545454534353434}&quot;</span>
<span class="token punctuation">}</span>
</pre><h4 class="mume-header" id="get-keys-search">Get Keys - search</h4>

<p>Get one or more specific values from the store and also reset the TTL of those keys.</p>
<p>URL <code>(get)</code> :</p>
<pre data-role="codeBlock" data-info="php" class="language-php">http<span class="token punctuation">:</span><span class="token comment">//localhost:8000/values?keys=key1,key2</span>
</pre><p>Output Response :</p>
<pre data-role="codeBlock" data-info="json" class="language-json"><span class="token punctuation">{</span>
    <span class="token property">&quot;status&quot;</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">&quot;message&quot;</span><span class="token operator">:</span> <span class="token string">&quot;Keys List&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;keys&quot;</span><span class="token operator">:</span> <span class="token string">&quot;{key1: 8912u9182u981212bjh12bjh12j1243434341111111, key2: 123343423232332323322434545454534353434}&quot;</span>
<span class="token punctuation">}</span>
</pre><h4 class="mume-header" id="update-keys-">Update Keys -</h4>

<p>Update a value in the store and also reset the TTL.</p>
<p>URL <code>(patch)</code> :</p>
<pre data-role="codeBlock" data-info="php" class="language-php">http<span class="token punctuation">:</span><span class="token comment">//localhost:8000/values</span>
</pre><p>Input Data :</p>
<pre data-role="codeBlock" data-info="json" class="language-json"><span class="token punctuation">{</span>
    <span class="token property">&quot;key1&quot;</span><span class="token operator">:</span> <span class="token string">&quot;8912u9182u981212bjh12bjh12j1243434340000000&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;key2&quot;</span><span class="token operator">:</span> <span class="token string">&quot;901290192012019280198298328930918098080001&quot;</span>
<span class="token punctuation">}</span>
</pre><p>Output Response :</p>
<pre data-role="codeBlock" data-info="json" class="language-json"><span class="token punctuation">{</span>
    <span class="token property">&quot;status&quot;</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">&quot;message&quot;</span><span class="token operator">:</span> <span class="token string">&quot;Keys have been updated !!&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;keys&quot;</span><span class="token operator">:</span> <span class="token string">&quot;{key1: 8912u9182u981212bjh12bjh12j1243434340000000, key2: 901290192012019280198298328930918098080001}&quot;</span>
<span class="token punctuation">}</span>
</pre><h2 class="mume-header" id="any-issues">Any issues</h2>

<hr>
<p>Add in issues or mail me at <a href="mailto:manirujjamanakash@gmail.com">manirujjamanakash@gmail.com</a></p>

        </div>
    </body>
</html>